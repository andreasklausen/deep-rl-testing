# Q-learning introduction

This example is based on a [tutorial](https://pythonprogramming.net/training-deep-q-learning-dqn-reinforcement-learning-python-tutorial/?completed=/deep-q-learning-dqn-reinforcement-learning-python-tutorial/) made by sentdex on YouTube.

Make this anaconda environment
```
conda create -n sentdexrl python==3.7 numpy==1.16.1, matplotlib==3.0.2 pylint
conda acivate sentdexrl
pip install gym
```