""" This example demonstrates how to use the FMU.get*() and FMU.set*() functions
to set custom input and control the simulation
This code is taken from FMPy's homepage with custom_input
https://github.com/CATIA-Systems/FMPy/blob/master/fmpy/examples/custom_input.py
"""

from fmpy import read_model_description, extract
from fmpy.fmi2 import FMU2Slave
from fmpy.util import plot_result
import numpy as np
import shutil
import matplotlib.pyplot as plt

class Model:
    def __init__(self):
        self.fmu_filename = 'test_integrate.fmu'
        self.start_time = 0.0
        self.stop_time = 10.0
        self.step_size = 2e-2

        self.observation_space_high = 1.1
        self.observation_space_low = -1.1
        self.action_space_n = 3

        self.model_description = read_model_description(self.fmu_filename)

        self.vrs = {}
        for variable in self.model_description.modelVariables:
            self.vrs[variable.name] = variable.valueReference
        self.vr_input = self.vrs['u']
        self.vr_output = self.vrs['y']
        self.vr_initial_integrator = self.vrs['integrator_initial']

        self.unzipdir = extract(self.fmu_filename)

        self.fmu = FMU2Slave(guid=self.model_description.guid,
                        unzipDirectory=self.unzipdir,
                        modelIdentifier=self.model_description.coSimulation.modelIdentifier,
                        instanceName='instance1')

        self.fmu.instantiate(loggingOn=False)
        self._initialize()

    def step(self, action):
        if action == 0 and self.output_value >= -1.0:
            input_value = -1.0
        elif action == 1:
            input_value = 0.0
        elif action == 2 and self.output_value <= 1.0:
            input_value = 1.0
        else:
            input_value = 0.0

        self.fmu.setReal(vr=[self.vr_input], value=[input_value])
        self.fmu.doStep(currentCommunicationPoint=self.time, communicationStepSize=self.step_size)
        self.output_value, = self.fmu.getReal([self.vr_output])
        self.time += self.step_size

        if self.time >= self.stop_time:
            done = True
        else:
            done = False

        if -0.1 < self.output_value < 0.1:
            reward = 1.0
        else:
            reward = 0.0
        return self.output_value, reward, done

    def reset(self):
        self.fmu.reset()
        self._initialize()
        return self.output_value

    def _initialize(self):
        self.time = self.start_time
        self.fmu.setupExperiment(startTime=self.time)
        self.fmu.enterInitializationMode()
        random_temp = 0.8*np.random.rand() + 0.2
        self.output_value = [random_temp, -random_temp][np.random.randint(0, 2)]
        self.fmu.setReal([self.vr_initial_integrator], [self.output_value])
        self.fmu.exitInitializationMode()

    def close(self):
        self.fmu.terminate()
        self.fmu.freeInstance()
        shutil.rmtree(self.unzipdir, ignore_errors=True)


if __name__ == "__main__":
    model = Model()
    LEARNING_RATE = 0.1
    DISCOUNT = 0.95
    EPISODES = 100

    epsilon = 1.0
    START_EPSILON_DECAYING = 1
    END_EPSILON_DECAYING = EPISODES//2
    epsilon_decay_value = epsilon/(END_EPSILON_DECAYING - START_EPSILON_DECAYING)

    discrete_observation_size = 20
    discrete_observation_window_size = (model.observation_space_high - model.observation_space_low)/discrete_observation_size

    q_table = np.random.uniform(low=-2, high=-0.5, size=[discrete_observation_size] + [model.action_space_n])

    max_q_episode_list = []

    def get_discrete_state(state):
        discrete_state = (state - model.observation_space_low)/discrete_observation_window_size
        return tuple([int(discrete_state),])  # we use this tuple to look up the 3 Q values for the available actions in the q-table

    for episode in range(EPISODES):
        discrete_state = get_discrete_state(model.reset())
        done = False
        max_q_episode = -1000
        while not done:
            if np.random.rand() > epsilon:
                action = np.argmax(q_table[discrete_state])
            else:
                action = np.random.randint(0, model.action_space_n)

            new_state, reward, done = model.step(action)
            new_discrete_state = get_discrete_state(new_state)
            if not done:
                max_future_q = np.max(q_table[new_discrete_state])
                current_q = q_table[discrete_state + (action,)]
                new_q = (1 - LEARNING_RATE) * current_q + LEARNING_RATE * (reward + DISCOUNT * max_future_q)
                q_table[discrete_state + (action,)] = new_q
                if new_q > max_q_episode:
                    max_q_episode = new_q
            discrete_state = new_discrete_state
        if END_EPSILON_DECAYING >= episode >= START_EPSILON_DECAYING:
            epsilon -= epsilon_decay_value
        print(f'Maximum new_q for episode {episode} is {max_q_episode}')
        max_q_episode_list.append(max_q_episode)
    model.close()

fig, ax = plt.subplots()
ax.plot(max_q_episode_list)
plt.show()