# Q-learning introduction

This example is based on a [tutorial](https://pythonprogramming.net/q-learning-reinforcement-learning-python-tutorial/) made by sentdex on YouTube.

Make this anaconda environment
```
conda create -n sentdexrl python==3.7 numpy==1.16.1, matplotlib==3.0.2 pylint
conda acivate sentdexrl
pip install gym
```