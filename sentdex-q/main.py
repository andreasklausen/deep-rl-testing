# objective is to get the cart to the flag.
# for now, let's just move randomly:

import gym
import numpy as np

# Environment
env = gym.make("MountainCar-v0")

# Hyper parameters
LEARNING_RATE = 0.1
DISCOUNT = 0.95
EPISODES = 25000
SHOW_EVERY = 500

# Size of discrete positions
DISCRETE_OS_SIZE = [20, 20]
discrete_os_win_size = (env.observation_space.high - env.observation_space.low)/DISCRETE_OS_SIZE

# Exploration settings
epsilon = 1  # not a constant, qoing to be decayed
START_EPSILON_DECAYING = 1
END_EPSILON_DECAYING = EPISODES//2
epsilon_decay_value = epsilon/(END_EPSILON_DECAYING - START_EPSILON_DECAYING)

# Initialize q table
q_table = np.random.uniform(low=-2, high=-0.5, size=(DISCRETE_OS_SIZE + [env.action_space.n]))

# Get the discrete state from the continuous state
def get_discrete_state(state):
    discrete_state = (state - env.observation_space.low)/discrete_os_win_size
    return tuple(discrete_state.astype(np.int))  # we use this tuple to look up the 3 Q values for the available actions in the q-table

# Run through all the episodes
for episode in range(EPISODES):
    discrete_state = get_discrete_state(env.reset())
    done = False
    
    # Save the maxmimum Q encountered during an epiode
    max_q_episode = -1000

    # Epsilon chech
    while not done:
        if np.random.random() > epsilon:
            # Get action from Q table
            action = np.argmax(q_table[discrete_state])
        else:
            # Get random action
            action = np.random.randint(0, env.action_space.n)

        # Make an action
        new_state, reward, done, _ = env.step(action)

        # Set new discrete state
        new_discrete_state = get_discrete_state(new_state)

        # Render if necessary
        if episode % SHOW_EVERY == 0:
            env.render()

        # If simulation did not end yet after last step - update Q table
        if not done:
            # Maximum possible Q value in next step (for new state)
            max_future_q = np.max(q_table[new_discrete_state])

            # Current Q value (for current state and performed action)
            current_q = q_table[discrete_state + (action,)]

            # And here's our equation for a new Q value for current state and action
            new_q = (1 - LEARNING_RATE) * current_q + LEARNING_RATE * (reward + DISCOUNT * max_future_q)

            # Update Q table with new Q value
            q_table[discrete_state + (action,)] = new_q
            
            # Save maximum value of new q
            if new_q > max_q_episode:
                max_q_episode = new_q

        # Simulation ended (for any reson) - if goal position is achived - update Q value with reward directly
        elif new_state[0] >= env.goal_position:
            q_table[discrete_state + (action,)] = 0  # Here, 0 is maximum reward

        # Set the discrete state
        discrete_state = new_discrete_state

    # Decaying is being done every episode if episode number is within decaying range
    if END_EPSILON_DECAYING >= episode >= START_EPSILON_DECAYING:
        epsilon -= epsilon_decay_value
        
    # Print maximum Q for this episode
    print(f'Maximum new_q for episode {episode} is {max_q_episode}')

env.close()
